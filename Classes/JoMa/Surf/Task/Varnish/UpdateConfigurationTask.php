<?php
namespace JoMa\Surf\Task\Varnish;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "JoMa.Surf".             *
 *                                                                        *
 *                                                                        */

use TYPO3\Surf\Domain\Model\Node;
use TYPO3\Surf\Domain\Model\Application;
use TYPO3\Surf\Domain\Model\Deployment;
use JoMa\Surf\Domain\Model\VarnishContext;
use JoMa\Surf\Domain\Model\VarnishConfiguration;
use TYPO3\Flow\Annotations as Flow;

/**
 * Task for reconfiguring in Varnish, should be used for Varnish 3.x
 *
 * It takes the following options:
 *
 * * secretFile - path to the secret file, defaults to "/etc/varnish/secret"
 * * varnishadm - path to the varnishadm utility, defaults to "/usr/bin/varnishadm"
 * * varnishConfigurationFilePath - path to varnish configuration file, defaults to "/etc/varnish/default.vcl"
 */
class UpdateConfigurationTask extends \TYPO3\Surf\Domain\Model\Task {

	/**
	 * @Flow\Inject
	 * @var \JoMa\Surf\Domain\Service\VarnishService
	 */
	protected $varnishService;

	/**
	 * Execute this task
	 *
	 * @param \TYPO3\Surf\Domain\Model\Node $node
	 * @param \TYPO3\Surf\Domain\Model\Application $application
	 * @param \TYPO3\Surf\Domain\Model\Deployment $deployment
	 * @param array $options
	 * @return void
	 */
	public function execute(Node $node, Application $application, Deployment $deployment, array $options = array()) {
        $secretFile = (isset($options['secretFile']) ? $options['secretFile'] : '/etc/varnish/secret');
        $varnishadm = (isset($options['varnishadm']) ? $options['varnishadm'] : '/usr/bin/varnishadm');
        $varnishConfigurationFilePath = (isset($options['varnishConfigurationFilePath']) ? $options['varnishConfigurationFilePath'] : '/etc/varnish/default.vcl');

        $varnishContext = new VarnishContext($deployment, $node, $secretFile, $varnishadm);
        $varnishConfiguration = new VarnishConfiguration($varnishConfigurationFilePath);

        $this->varnishService->replaceActiveConfigurationByConfiguration($varnishContext, $varnishConfiguration);
	}

	/**
	 * Simulate this task
	 *
	 * @param Node $node
	 * @param Application $application
	 * @param Deployment $deployment
	 * @param array $options
	 * @return void
	 */
	public function simulate(Node $node, Application $application, Deployment $deployment, array $options = array()) {
        $this->execute($node, $application, $deployment, $options);
	}

}
