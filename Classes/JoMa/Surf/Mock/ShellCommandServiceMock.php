<?php
namespace JoMa\Surf\Mock;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "JoMa.Surf".             *
 *                                                                        *
 *                                                                        */

use TYPO3\Surf\Domain\Model\Deployment;
use TYPO3\Surf\Domain\Model\Node;
use \TYPO3\Surf\Domain\Service\ShellCommandService;

/**
 * Shell Command Service Mock
 *
 * This mock exists due to issues with mocking method parameters in PHPUnit
 * See https://github.com/sebastianbergmann/phpunit-mock-objects/pull/142
 */
class ShellCommandServiceMock extends ShellCommandService {

    /**
     * @var \TYPO3\Flow\Tests\UnitTestCase
     */
    protected $testCase;

    /**
     * @var array
     */
    protected $commands;

    /**
     * @param \TYPO3\Flow\Tests\UnitTestCase $testCase
     * @param array $commands
     */
    function __construct($testCase, $commands) {
        $this->testCase = $testCase;
        $this->commands = $commands;
    }

    /**
     * @param string $command
     * @param Node $node
     * @param Deployment $deployment
     * @param bool $ignoreErrors
     * @param bool $logOutput
     * @return bool|mixed
     */
    public function executeOrSimulate($command, Node $node, Deployment $deployment, $ignoreErrors = FALSE, $logOutput = TRUE) {
        $commandAndReturnValue = array_shift($this->commands);
        $this->testCase->assertEquals($commandAndReturnValue['command'], $command);
        return $commandAndReturnValue['return'];
    }

}