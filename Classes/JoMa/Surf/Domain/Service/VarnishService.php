<?php
namespace JoMa\Surf\Domain\Service;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "JoMa.Surf".             *
 *                                                                        *
 *                                                                        */

use JoMa\Surf\Domain\Model\VarnishConfiguration;
use JoMa\Surf\Domain\Model\VarnishContext;
use JoMa\Surf\Exception\SurfException;
use TYPO3\Flow\Annotations as Flow;

/**
 * Varnish Service
 */
class VarnishService {

    /**
     * @Flow\Inject
     * @var \TYPO3\Surf\Domain\Service\ShellCommandService
     */
    protected $shell;

    /**
     * @param VarnishContext $varnishContext
     * @return array of VarnishConfigurations
     */
    public function getConfigurations($varnishContext) {
        $configurations = array();
        $result = $this->runVarnishCommand($varnishContext, 'vcl.list');
        $results = explode("\n", $result);
        foreach ($results as $result) {
            $configuration = $this->buildConfiguration($result);
            if ($configuration instanceof VarnishConfiguration) {
                $configurations[] = $configuration;
            }
        }
        return $configurations;
    }


    /**
     * @param string $settings
     * @return VarnishConfiguration|NULL
     * @throws \JoMa\Surf\Exception\SurfException
     */
    protected function buildConfiguration($settings) {
        $splitSettings = preg_split("/[\s]+/", $settings);
        $configuration = NULL;
        if (count($splitSettings) === 3) {
            $configuration = new VarnishConfiguration();
            $configuration->setStatus($splitSettings[0])
                ->setNumberOfReferences($splitSettings[1])
                ->setName($splitSettings[2]);
        }
        return $configuration;
    }

    /**
     * @param VarnishContext $varnishContext
     * @param VarnishConfiguration $varnishConfiguration
     */
    public function replaceActiveConfigurationByConfiguration($varnishContext, $varnishConfiguration) {
        $activeConfiguration = $this->getActiveConfiguration($varnishContext);
        $this->runVarnishCommand($varnishContext, 'vcl.load', array($varnishConfiguration->getName(), $varnishConfiguration->getFilePath()));
        $this->runVarnishCommand($varnishContext, 'vcl.use', array($varnishConfiguration->getName()));
        $this->runVarnishCommand($varnishContext, 'vcl.discard', array($activeConfiguration->getName()));
        $this->runVarnishCommand($varnishContext, 'ban.url', array('.'));
    }

    /**
     * @param VarnishContext $varnishContext
     * @return VarnishConfiguration
     */
    protected function getActiveConfiguration($varnishContext) {
        $configurations = $this->getConfigurations($varnishContext);
        foreach ($configurations as $configuration) { /** @var VarnishConfiguration $configuration */
            if ($configuration->getStatus() === 'active') {
                return $configuration;
            }
        }
        return new VarnishConfiguration();
    }

    /**
     * @param VarnishContext $varnishContext
     * @param string $command
     * @param array $parameters
     * @return string
     */
    protected function runVarnishCommand($varnishContext, $command, $parameters = array()) {
        $command = sprintf(
            "%s -S %s -T 127.0.0.1:6082 %s %s",
            $varnishContext->getVarnishadmCommand(),
            $varnishContext->getSecretFilePath(),
            $command,
            implode(" ", $parameters)
        );
        return $this->shell->executeOrSimulate($command, $varnishContext->getNode(), $varnishContext->getDeployment());
    }

}
