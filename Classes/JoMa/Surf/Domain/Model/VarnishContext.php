<?php
namespace JoMa\Surf\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "JoMa.Surf".             *
 *                                                                        *
 *                                                                        */

use TYPO3\Surf\Domain\Model\Node;
use TYPO3\Surf\Domain\Model\Deployment;

use TYPO3\Flow\Annotations as Flow;

/**
 * Varnish Context
 */
class VarnishContext {

    /**
     * @var Deployment
     */
    protected $deployment;

    /**
     * @var Node
     */
    protected $node;

    /**
     * @var string
     */
    protected $varnishadmCommand;

    /**
     * @var string
     */
    protected $secretFilePath;

    /**
     * @param Deployment $deployment
     * @param Node $node
     * @param string $secretFilePath
     * @param string $varnishadmCommand
     */
    function __construct($deployment, $node, $secretFilePath, $varnishadmCommand) {
        $this->deployment = $deployment;
        $this->node = $node;
        $this->secretFilePath = $secretFilePath;
        $this->varnishadmCommand = $varnishadmCommand;
    }

    /**
     * @param \TYPO3\Surf\Domain\Model\Deployment $deployment
     */
    public function setDeployment($deployment) {
        $this->deployment = $deployment;
    }

    /**
     * @return \TYPO3\Surf\Domain\Model\Deployment
     */
    public function getDeployment() {
        return $this->deployment;
    }

    /**
     * @param \TYPO3\Surf\Domain\Model\Node $node
     */
    public function setNode($node) {
        $this->node = $node;
    }

    /**
     * @return \TYPO3\Surf\Domain\Model\Node
     */
    public function getNode() {
        return $this->node;
    }

    /**
     * @param string $secretFilePath
     */
    public function setSecret($secretFilePath) {
        $this->secretFilePath = $secretFilePath;
    }

    /**
     * @return string
     */
    public function getSecretFilePath() {
        return $this->secretFilePath;
    }

    /**
     * @param string $varnishadmCommand
     */
    public function setVarnishadmCommand($varnishadmCommand) {
        $this->varnishadmCommand = $varnishadmCommand;
    }

    /**
     * @return string
     */
    public function getVarnishadmCommand() {
        return $this->varnishadmCommand;
    }

}
