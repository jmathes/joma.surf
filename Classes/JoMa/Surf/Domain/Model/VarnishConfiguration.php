<?php
namespace JoMa\Surf\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "JoMa.Surf".             *
 *                                                                        *
 *                                                                        */

use JoMa\Surf\Exception\SurfException;
use TYPO3\Flow\Annotations as Flow;

/**
 * Varnish Configuration
 */
class VarnishConfiguration {

    /**
     * @var string
     */
    protected $status;

    /**
     * @var int
     */
    protected $numberOfReferences;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $filePath;

    /**
     * @param $filePath
     */
    function __construct($filePath = '') {
        $this->setFilePath($filePath);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param int $numberOfReferences
     * @return $this
     */
    public function setNumberOfReferences($numberOfReferences) {
        $this->numberOfReferences = $numberOfReferences;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfReferences() {
        return $this->numberOfReferences;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param string $filePath
     * @throws SurfException
     */
    public function setFilePath($filePath) {
        if (!empty($filePath) && !file_exists($filePath)) {
            throw new SurfException('No valid file path :' . $filePath, 1396337396);
        }
        $this->filePath = $filePath;
        if (!empty($filePath) && empty($this->name)) {
            $pathParts = pathinfo($this->getFilePath());
            $this->name = $pathParts['filename'] . '_' . time();
        }
    }

    /**
     * @return string
     */
    public function getFilePath() {
        return $this->filePath;
    }

}
