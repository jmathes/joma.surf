<?php
namespace JoMa\Surf\Exception;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "JoMa.Surf".             *
 *                                                                        *
 *                                                                        */

/**
 * Surf Exception
 */
class SurfException extends \Exception {

}