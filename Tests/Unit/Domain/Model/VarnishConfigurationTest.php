<?php
namespace JoMa\Surf\Tests\Unit\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "JoMa.Surf".             *
 *                                                                        *
 *                                                                        */

use JoMa\Surf\Domain\Model\VarnishConfiguration;
use TYPO3\Flow\Tests\UnitTestCase;
use org\bovigo\vfs;

/**
 * Varnish Configuration test case
 */
class VarnishConfigurationTest extends UnitTestCase {

    /**
     * @var array
     */
    protected $directoryStructure = array(
        'etc' => array(
            'varnish' => array(
                'default.vcl' => 'configuration...'
            )
        )
    );

    public function setUp() {
        vfs\vfsStream::setup('root', NULL, $this->directoryStructure);
    }

    /**
     * @test
     */
    public function noExceptionIsThrownIfNoFilePathIsSetAsConstructorArgument() {
        try {
            $varnishConfiguration = new VarnishConfiguration();
            $this->assertInstanceOf('\JoMa\Surf\Domain\Model\VarnishConfiguration', $varnishConfiguration);
        } catch (\Exception $exception) {
            $this->fail('Exception should not be thrown!');
        }
    }

    /**
     * @test
     * @expectedException \JoMa\Surf\Exception\SurfException
     */
    public function exceptionIsThrownIfFilePathDoesNotExist() {
        new VarnishConfiguration(vfs\vfsStream::url('etc/varnish/config.vcl'));
    }

    /**
     * @test
     */
    public function constructorSetsFilePath() {
        $proxyClass = $this->buildAccessibleProxy('\JoMa\Surf\Domain\Model\VarnishConfiguration');
        $expected = vfs\vfsStream::url('etc/varnish/default.vcl');
        $proxy = new $proxyClass($expected);
        $actual = $proxy->_get('filePath');
        $this->assertSame($expected, $actual);
    }

    /**
     * @test
     */
    public function constructorSetsDefaultConfigurationNameByFilePath() {
        $proxyClass = $this->buildAccessibleProxy('\JoMa\Surf\Domain\Model\VarnishConfiguration');
        $proxy = new $proxyClass(vfs\vfsStream::url('etc/varnish/default.vcl'));
        $expected = 'default';
        $actual = $proxy->_get('name');
        $this->assertStringStartsWith($expected, $actual);
    }

    /**
     * @test
     */
    public function setNameOverwritesNameWhichIsSetByConstructor() {
        $proxyClass = $this->buildAccessibleProxy('\JoMa\Surf\Domain\Model\VarnishConfiguration');
        $proxy = new $proxyClass(vfs\vfsStream::url('etc/varnish/default.vcl'));
        $expected = 'configuration';
        $proxy->setName($expected);
        $actual = $proxy->_get('name');
        $this->assertStringStartsWith($expected, $actual);
    }


}
