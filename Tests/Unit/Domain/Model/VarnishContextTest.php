<?php
namespace JoMa\Surf\Tests\Unit\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "JoMa.Surf".             *
 *                                                                        *
 *                                                                        */

use JoMa\Surf\Domain\Model\VarnishContext;
use TYPO3\Flow\Tests\UnitTestCase;

/**
 * Varnish Context test case
 */
class VarnishContextTest extends UnitTestCase {

    /**
     * @test
     */
    public function constructorSetsProperties() {
        $nodeMock = $this->getMockBuilder('\TYPO3\Surf\Domain\Model\Node')
            ->disableOriginalConstructor()
            ->getMock();
        $deploymentMock = $this->getMockBuilder('\TYPO3\Surf\Domain\Model\Deployment')
            ->disableOriginalConstructor()
            ->getMock();
        $secretFilePath = '/etc/varnish/secret';
        $varnishadmCommand = '/usr/bin/varnishadm';

        $varnishContext = new VarnishContext($deploymentMock, $nodeMock, $secretFilePath, $varnishadmCommand);
        $this->assertInstanceOf('\TYPO3\Surf\Domain\Model\Node', $varnishContext->getNode());
        $this->assertInstanceOf('\TYPO3\Surf\Domain\Model\Deployment', $varnishContext->getDeployment());
        $this->assertEquals($secretFilePath, $varnishContext->getSecretFilePath());
        $this->assertEquals($varnishadmCommand, $varnishContext->getVarnishadmCommand());
    }

}
