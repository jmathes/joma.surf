<?php
namespace JoMa\Surf\Tests\Unit\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "JoMa.Surf".             *
 *                                                                        *
 *                                                                        */

use JoMa\Surf\Domain\Model\VarnishConfiguration;
use JoMa\Surf\Domain\Service\VarnishService;
use TYPO3\Flow\Tests\UnitTestCase;
use org\bovigo\vfs;

/**
 * Varnish Service test case
 */
class VarnishServiceTest extends UnitTestCase {

    /**
     * @var \TYPO3\Surf\Domain\Model\Node
     */
    protected $nodeMock;

    /**
     * @var \TYPO3\Surf\Domain\Model\Deployment
     */
    protected $deploymentMock;

    /**
     * @return void
     */
    public function setUp() {
        $this->nodeMock = $this->getMockBuilder('\TYPO3\Surf\Domain\Model\Node')
            ->disableOriginalConstructor()
            ->getMock();
        $this->deploymentMock = $this->getMockBuilder('\TYPO3\Surf\Domain\Model\Deployment')
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @return array
     */
    public function getConfigurationsReturnsValidArrayOfVarnishConfigurationsDataProvider() {
        $varnishConfiguration1 = new VarnishConfiguration();
        $varnishConfiguration1->setName('boot')
            ->setStatus('active')
            ->setNumberOfReferences(0);
        $varnishConfiguration2 = new VarnishConfiguration();
        $varnishConfiguration2->setName('boot')
            ->setStatus('available')
            ->setNumberOfReferences(0);
        $varnishConfiguration3 = new VarnishConfiguration();
        $varnishConfiguration3->setName('configuration_1234567890')
            ->setStatus('active')
            ->setNumberOfReferences(0);
        $varnishConfiguration4 = new VarnishConfiguration();
        $varnishConfiguration4->setName('configuration_1234567891')
            ->setStatus('available')
            ->setNumberOfReferences(0);
        return array(
            'threeConfigurationsOneActive' => array(
                'settings' => implode("\n", array(
                    'available       0 boot',
                    'active          0 configuration_1234567890',
                    'available       0 configuration_1234567891',
                )),
                'configurations' => array(
                    $varnishConfiguration2,
                    $varnishConfiguration3,
                    $varnishConfiguration4,
                )
            ),
        );
    }

    /**
     * @test
     * @dataProvider getConfigurationsReturnsValidArrayOfVarnishConfigurationsDataProvider
     */
    public function getConfigurationsReturnsValidArrayOfVarnishConfigurations($settings, $configurations) {
        $shellCommandServiceMock = $this->getMockBuilder('\TYPO3\Surf\Domain\Service\ShellCommandService')
            ->setMethods(array('executeOrSimulate'))
            ->getMock();
        $shellCommandServiceMock->expects($this->once())
            ->method('executeOrSimulate')
            ->with(
                $this->stringContains('/usr/bin/varnishadm -S /etc/varnish/secret -T 127.0.0.1:6082 vcl.list',
                $this->equalTo($this->nodeMock),
                $this->equalTo($this->deploymentMock)
            ))
            ->will($this->returnValue($settings));

        $varnishServiceProxyClass = $this->buildAccessibleProxy('\JoMa\Surf\Domain\Service\VarnishService');
        $varnishServiceProxy = new $varnishServiceProxyClass();
        $varnishServiceProxy->_set('shell', $shellCommandServiceMock);
        $actual = $varnishServiceProxy->getConfigurations($this->getVarnishContextMock());
        $this->assertEquals($configurations, $actual);
    }

    /**
     * @test
     */
    public function replaceActiveConfigurationByConfigurationRunsValidVarnishCommands() {
        $settings = implode("\n", array(
            'available       0 boot',
            'active          0 configuration_1234567890',
            'available       0 configuration_1234567891'));
        $expectedCommandsAndReturnValues = array(
            array(
                'command' => '/usr/bin/varnishadm -S /etc/varnish/secret -T 127.0.0.1:6082 vcl.list ',
                'return' => $settings
            ),
            array(
                'command' => '/usr/bin/varnishadm -S /etc/varnish/secret -T 127.0.0.1:6082 vcl.load configuration /etc/varnish/configuration.vcl',
                'return' => NULL
            ),
            array(
                'command' => '/usr/bin/varnishadm -S /etc/varnish/secret -T 127.0.0.1:6082 vcl.use configuration',
                'return' => NULL
            ),
            array(
                'command' => '/usr/bin/varnishadm -S /etc/varnish/secret -T 127.0.0.1:6082 vcl.discard configuration_1234567890',
                'return' => NULL
            ),
            array(
                'command' => '/usr/bin/varnishadm -S /etc/varnish/secret -T 127.0.0.1:6082 ban.url .',
                'return' => NULL
            )
        );

        $shellCommandServiceMock = new \JoMa\Surf\Mock\ShellCommandServiceMock($this, $expectedCommandsAndReturnValues);

        $configurationMock = $this->getMockBuilder('\JoMa\Surf\Domain\Model\VarnishConfiguration')
            ->setMethods(array('getName', 'getFilePath'))
            ->getMock();
        $configurationMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('configuration'));
        $configurationMock->expects($this->any())
            ->method('getFilePath')
            ->will($this->returnValue('/etc/varnish/configuration.vcl'));

        $varnishServiceProxyClass = $this->buildAccessibleProxy('\JoMa\Surf\Domain\Service\VarnishService');
        $varnishServiceProxy = new $varnishServiceProxyClass();
        $varnishServiceProxy->_set('shell', $shellCommandServiceMock);
        $varnishServiceProxy->replaceActiveConfigurationByConfiguration($this->getVarnishContextMock(), $configurationMock);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getVarnishContextMock() {
        $varnishContextMock = $this->getMockBuilder('\JoMa\Surf\Domain\Model\VarnishContext')
            ->disableOriginalConstructor()
            ->setMethods(array('getVarnishadmCommand', 'getSecretFilePath', 'getNode', 'getDeployment'))
            ->getMock();
        $varnishContextMock->expects($this->any())
            ->method('getVarnishadmCommand')
            ->will($this->returnValue('/usr/bin/varnishadm'));
        $varnishContextMock->expects($this->any())
            ->method('getNode')
            ->will($this->returnValue($this->nodeMock));
        $varnishContextMock->expects($this->any())
            ->method('getSecretFilePath')
            ->will($this->returnValue('/etc/varnish/secret'));
        $varnishContextMock->expects($this->any())
            ->method('getDeployment')
            ->will($this->returnValue($this->deploymentMock));
        return $varnishContextMock;
    }

}
